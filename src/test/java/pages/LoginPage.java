package pages;

import elements.DashboardBlock;
import elements.LoginBlock;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.htmlelements.loader.HtmlElementLoader;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;
import ru.yandex.qatools.htmlelements.pagefactory.CustomElementLocatorFactory;

/**
 * @author Angshuman Ray
 *         12/7/17
 */
public class LoginPage {

    private WebDriver driver;

    @FindBy(how = How.CLASS_NAME, using = "loginContainer_form")
    private LoginBlock loginBlock;

    @FindBy(how = How.CLASS_NAME, using = "dashboardContainer.flexDown.ng-scope")
    private DashboardBlock dashboardBlock;

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(driver)), this);
    }

   public void login(String userName, String password) throws InterruptedException {
       loginBlock.loginTo(userName,password);
       Thread.sleep(3000);
       System.out.println(dashboardBlock.getPendingDisptchfromDshboard());
       System.out.println("executed");

   }


}
