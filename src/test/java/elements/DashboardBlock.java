package elements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import ru.yandex.qatools.htmlelements.element.*;


/**
 * Created by QI-37 on 12-07-2017.
 */
public class DashboardBlock extends HtmlElement {

    @FindBy(how = How.CSS, using = ".listViewStatusTab.disableTxtSelect")
        public WebElement pendingDisptchLbl;

    public DashboardBlock(){

       // System.out.println(pendingDisptchLbl.getText());
        System.out.println("Initialized...............");
    }

    public String getPendingDisptchfromDshboard() {

        return pendingDisptchLbl.getText();
    }
}
