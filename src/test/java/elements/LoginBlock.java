package elements;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextInput;
import ru.yandex.qatools.htmlelements.element.Named;


/**
 * @author Angshuman Ray
 *         12/7/17
 */


public class LoginBlock extends HtmlElement {

    @FindBy(how = How.ID, using = "ts-username")
    public TextInput uname;

    @FindBy(how = How.ID, using = "ts-password")
    public TextInput paswd;

    @FindBy(how = How.CSS, using = ".alr.btn-positive")
    public Button loginBtn;



    public void setUsername(String userName) {

        uname.sendKeys(userName);

    }

    public void setPswrd(String pswrd) {

        paswd.sendKeys(pswrd);
    }

    public void clickLogin() {
        loginBtn.click();
    }


    public void loginTo(String userName, String pswrd) {
        try {

            setUsername(userName);
            setPswrd(pswrd);
            clickLogin();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
