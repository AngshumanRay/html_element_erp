package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

/**
 * Created by QI-37 on 12-07-2017.
 */
public class loginTest {

    private WebDriver driver = null;
    private LoginPage loginPage;


     @BeforeTest
    public void load() {

        System.setProperty("webdriver.chrome.driver", this.getClass().getClassLoader().getResource("chromedriver.exe").getFile());
        driver = new ChromeDriver();
        driver.get("http://mev-master.onesynq.com:8001/login");
         driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @Test
    public void sampleTest() throws InterruptedException {
        System.out.println("before login");

        loginPage = new LoginPage(driver);
        loginPage.login("dispatcher12@ura.com", "123456789");


    }

    /*@AfterTest
    public void closeDriver() {
        driver.quit();
    }
*/

}